import React, { Component } from 'react'
import { connect } from 'react-redux'
import ItemShoe from './ItemShoe'

class ListShoe extends Component {
  render() {
    return (
        <div className="row">
            {this.props.listShoe.map((item, index) => {
                return <ItemShoe shoe={item} key={index}/>
            })}
        </div>
    )
  }
}

let mapStateToProps = (state) => {
    return {
        listShoe: state.shoeReducer.listShoe,
    }
}

export default connect(mapStateToProps)(ListShoe);
