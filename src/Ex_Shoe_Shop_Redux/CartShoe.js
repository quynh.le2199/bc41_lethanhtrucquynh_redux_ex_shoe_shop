import React, { Component } from "react";
import { connect } from "react-redux";
import { CHANGE_QUANTITY, DELETE_SHOE } from "./redux/constant/shoeConstant";

class CartShoe extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>
            <img src={item.image} alt="" style={{ width: 50 }} />
          </td>
          <td>{item.price}</td>
          <td>
            <button
              onClick={() => { this.props.handleChangeQuantity(item.id, -1) }}
              className="btn px-1 mx-1 quantityButton"
            >
              -
            </button>
            <strong>{item.soLuong}</strong>
            <button onClick={() => { this.props.handleChangeQuantity(item.id, 1); }}
              className="btn px-1 mx-1 quantityButton"
            >
              +
            </button>
          </td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleDelete(item.id);
              }}
              className="btn btn-danger"
            >
              Delete
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="cartShoe">
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Image</th>
              <th>Unit price</th>
              <th>Quantity</th>
              <th>Total price</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (idShoe) => {
      dispatch({
        type: DELETE_SHOE,
        payload: idShoe,
      });
    },
    handleChangeQuantity: (idShoe, luaChon) => {
      dispatch({
        type: CHANGE_QUANTITY,
        payload: { idShoe, luaChon },
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartShoe);
