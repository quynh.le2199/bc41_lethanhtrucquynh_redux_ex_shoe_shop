import React, { Component } from 'react'
import { ADD_TO_CART } from './redux/constant/shoeConstant';
import { connect } from 'react-redux';

class ItemShoe extends Component {
  render() {
    let {image, name, price} = this.props.shoe;
    return (
      <div className="col-4 p-3 shoeItem">
          <div className="card border-primary px-2">
            <img className="card-img-top" src={image}/>
            <div className="card-body">
                <h5 className="card-title">{name}</h5>
                <p className="card-text itemPrice">${price}</p>
                <br />
                <button onClick={() => {this.props.handleAddToCart(this.props.shoe)}} className="btn btn-primary">Add to cart</button>
            </div>
          </div>
      </div>

    )
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handleAddToCart: (shoe) => {
      dispatch({
        type: ADD_TO_CART,
        payload: shoe,
      })
    }
  }
}

export default connect(null, mapDispatchToProps)(ItemShoe);
