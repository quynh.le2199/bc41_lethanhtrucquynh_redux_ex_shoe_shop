import React, { Component } from 'react'
import ListShoe from './ListShoe'
import CartShoe from './CartShoe'
import { connect } from 'react-redux'

class Ex_Shoe_Shop_Redux extends Component {
  render() {
    return (
      <div className='container'>
        <h2 className='my-3'>Ex_Shoe_Shop_Redux</h2>
        {this.props.cart.length > 0 && <CartShoe/>}
        <ListShoe/>
      </div>
    )
  }
}

let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.cart,
  }
}

export default connect(mapStateToProps)(Ex_Shoe_Shop_Redux);
